#This file was automatically generated
#on 30/11/2023 at 09:10:21

add_library(GPIO OBJECT 
            src/RelianceADC.cpp
            src/RelianceBox.cpp
            src/RelianceFSM.cpp
            src/RelianceLED.cpp
            src/RelianceOutput.cpp
            src/CircularBuffer.cpp
            src/Fifo.cpp
           )

target_include_directories(GPIO PUBLIC .)
