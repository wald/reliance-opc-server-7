#ifndef RELIANCELED_H
#define RELIANCELED_H

#include <stdint.h>
#include <map>
#include <vector>
#include <mutex>
#include <unistd.h>

/**
 *
 * @brief LED control for Reliance box
 * @author ignacio.asensi@cern.ch
 * @date November 2022
 **/

class RelianceLED {

public:

  static const uint32_t LED_RED = 28;
  static const uint32_t LED_YELLOW = 29;
  static const uint32_t LED_GREEN = 27;

  static const uint32_t MODE_INPUT=0;
  static const uint32_t MODE_OUTPUT=1;
  
  /**
   * Close the communication if any
   **/
  ~RelianceLED();
  
  /**
   * Get the single instance
   **/
  static RelianceLED * GetInstance();
 
  /**
   * Get the verbose mode
   * @return True if the verbose mode is enabled
   */
  bool GetVerbose();

  /**
   * Set the verbose mode
   * @param enable Enable the verbose mode if True
   */
  void SetVerbose(bool enable);

  /**
   * Change the state of the LED
   * @param wpi wiringPi number
   * @param enable turn on if true
   **/
  void Write(uint32_t wpi, bool enable);

  /**
   * Change the state of the LED
   * @param wpi wiringPi number
   * @return true if enabled
   **/
  bool Read(uint32_t wpi);

  /**
   * Turn On one LED
   * @param led LED to turn on
   **/
  void TurnOn(uint32_t led);

  /**
   * Turn Off one LED
   * @param led LED to turn off
   **/
  void TurnOff(uint32_t led);

 private:

  /**
   * Create an empty RelianceLED.
   * Private to force users to use the Singleton.
   **/
  RelianceLED();

  bool Open();

  void Close();

  void SetMode(uint32_t wpi, uint32_t mode);

  uint32_t m_pi_base;

  uint32_t m_gpio_base;

  std::vector<int8_t> m_pin_to_gpio;

  std::vector<uint8_t> m_gpio_to_set;

  std::vector<uint8_t> m_gpio_to_clr;

  std::vector <uint8_t> m_gpio_to_lev;

  std::vector <uint8_t> m_gpio_to_sel;

  std::vector <uint8_t> m_gpio_to_shift;
  
  uint32_t * m_gpio;

  bool m_verbose;

  int32_t m_fd;
  
  static RelianceLED * m_me; //!< singleton pointer to be returned by RelianceLED::GetInstance

  std::mutex m_lock;

};

#endif
