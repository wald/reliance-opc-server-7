#ifndef RELIANCEADC_H
#define RELIANCEADC_H

#include <stdint.h>
#include <map>
#include <vector>
#include <mutex>
#include <unistd.h>

/**
 *
 * @brief Read-out and control of the ADS1115 on the Reliance cards over I2C
 * @author carlos.solans@cern.ch
 * @date August 2021
 **/

class RelianceADC {

public:

  static const uint32_t DEFAULT_I2C_ADDRESS=0x49;
  static const uint32_t SINGLE_ENDED=0;   /**< ADC mode : single ended */
  static const uint32_t DIFFERENTIAL=1;   /**< ADC mode : differential mode */

  static const uint32_t GAIN_TWOTHIRDS = 0;
  static const uint32_t GAIN_ONE = 1;
  static const uint32_t GAIN_TWO = 2;
  static const uint32_t GAIN_FOUR = 3;
  static const uint32_t GAIN_EIGHT = 4;
  static const uint32_t GAIN_SIXTEEN = 5;

  static const uint16_t CONFIG_CQUE_MASK=0x0003;
  static const uint16_t CONFIG_CQUE_1CON=0x0000;
  static const uint16_t CONFIG_CQUE_2CON=0x0001;
  static const uint16_t CONFIG_CQUE_4CON=0x0002;
  static const uint16_t CONFIG_CQUE_NONE=0x0003;

  static const uint16_t CONFIG_CLAT_MASK =0x0004;
  static const uint16_t CONFIG_CLAT_LATCH=0x0004;
  static const uint16_t CONFIG_CLAT_NOLAT=0x0000;

  static const uint16_t CONFIG_CPOL_MASK =0x0008;
  static const uint16_t CONFIG_CPOL_ACTHI=0x0008;
  static const uint16_t CONFIG_CPOL_ACTLO=0x0000;

  static const uint16_t CONFIG_CMOD_MASK=0x0010;
  static const uint16_t CONFIG_CMOD_TRAD=0x0010;
  static const uint16_t CONFIG_CMOD_WIND=0x0000;
  
  static const uint16_t CONFIG_DR_MASK   =0x00E0;  
  static const uint16_t CONFIG_DR_128SPS =0x0000;  // 128 samples per second
  static const uint16_t CONFIG_DR_250SPS =0x0020;  // 250 samples per second
  static const uint16_t CONFIG_DR_490SPS =0x0040;  // 490 samples per second
  static const uint16_t CONFIG_DR_920SPS =0x0060;  // 920 samples per second
  static const uint16_t CONFIG_DR_1600SPS=0x0080;  // 1600 samples per second (default)
  static const uint16_t CONFIG_DR_2400SPS=0x00A0;  // 2400 samples per second
  static const uint16_t CONFIG_DR_3300SPS=0x00C0;  // 3300 samples per second

  static const uint16_t CONFIG_MODE_MASK=0x0100;
  static const uint16_t CONFIG_MODE_CONT=0x0000;  // Continuous conversion mode
  static const uint16_t CONFIG_MODE_SINGLE=0x0100;  // Power-down single-shot mode (default)
    
  static const uint16_t CONFIG_PGA_MASK=0x0E00;
  static const uint16_t CONFIG_PGA_G23 =0x0000;  // +/-6.144V range = Gain 2/3
  static const uint16_t CONFIG_PGA_G1  =0x0200;  // +/-4.096V range = Gain 1
  static const uint16_t CONFIG_PGA_G2  =0x0400;  // +/-2.048V range = Gain 2 (default)
  static const uint16_t CONFIG_PGA_G4  =0x0600;  // +/-1.024V range = Gain 4
  static const uint16_t CONFIG_PGA_G8  =0x0800;  // +/-0.512V range = Gain 8
  static const uint16_t CONFIG_PGA_G16 =0x0A00;  // +/-0.256V range = Gain 16

  static const uint16_t CONFIG_MUX_MASK    =0x7000;
  static const uint16_t CONFIG_MUX_DIFF_0_1=0x0000;  // Differential P = AIN0, N = AIN1 (default)
  static const uint16_t CONFIG_MUX_DIFF_0_3=0x1000;  // Differential P = AIN0, N = AIN3
  static const uint16_t CONFIG_MUX_DIFF_1_3=0x2000;  // Differential P = AIN1, N = AIN3
  static const uint16_t CONFIG_MUX_DIFF_2_3=0x3000;  // Differential P = AIN2, N = AIN3
  static const uint16_t CONFIG_MUX_SINGLE_0=0x4000;  // Single-ended AIN0
  static const uint16_t CONFIG_MUX_SINGLE_1=0x5000;  // Single-ended AIN1
  static const uint16_t CONFIG_MUX_SINGLE_2=0x6000;  // Single-ended AIN2
  static const uint16_t CONFIG_MUX_SINGLE_3=0x7000;  // Single-ended AIN3

  static const uint16_t CONFIG_OS_MASK   =0x8000;
  static const uint16_t CONFIG_OS_SINGLE =0x8000;  // Write: Set to start a single-conversion
  static const uint16_t CONFIG_OS_BUSY   =0x0000;  // Read: Bit = 0 when conversion is in progress
  static const uint16_t CONFIG_OS_NOTBUSY=0x8000;  // Read: Bit = 1 when device is not performing a conversion

  static const uint16_t POINTER_MASK      = 0x03;
  static const uint16_t POINTER_CONVERT   = 0x00;
  static const uint16_t POINTER_CONFIG    = 0x01;
  static const uint16_t POINTER_LOWTHRESH = 0x02;
  static const uint16_t POINTER_HITHRESH  = 0x03;

  static const uint16_t ADS1015_CONVERSIONDELAY = 1;
  static const uint16_t ADS1115_CONVERSIONDELAY = 8;
  static const uint16_t ADS1015_BITSHIFT = 4;
  static const uint16_t ADS1115_BITSHIFT = 0;

  /**
   * Close the communication if any
   **/
  ~RelianceADC();
  
  /**
   * Get the single instance
   **/
  static RelianceADC * GetInstance();
  
  /**
   * Get the verbose mode
   * @return True if the verbose mode is enabled
   */
  bool GetVerbose();

  /**
   * @brief Get IC2 address.
   * @return address
   **/
  uint32_t GetI2CAddress();

  /**
   * Calibration constants are used to tranform the ADC counts into resistance
   * using a polynomial function. The constants are the factors of each term.
   * f(x,p0,p1,p2,...) = p0*x^0 + p1*x^2 + p2*x^2 + ...
   * @brief Get the calibration constant for a given index
   * @param idx The degree of the polynomial: idx=0 means order 0
   **/
  float GetCalib(uint32_t gain, uint32_t idx);
  
  /**
   * Set the verbose mode
   * @param enable Enable the verbose mode if True
   */
  void SetVerbose(bool enable);

  /**
   * @brief Set IC2 address.
   * @param address IC2 Address. 
   **/
  void SetI2CAddress(uint32_t address);
  
  /**
   * The resistance is calculated by applying a polynomial function to the ADC.
   * The coefficients for each degree of the polynomial function can be changed with this method.
   * f(x,p0,p1,p2,...) = p0*x^0 + p1*x^2 + p2*x^2 + ...
   * @brief Change the calibration constant for a given degree of a polynomial function used to convert the ADC to kOhm.
   * @param gain the gain of the ADC
   * @param idx The degree of the polynomial: idx=0 means order 0
   * @param cc The value of the calibration constant
   */
  void SetCalib(uint32_t gain, uint32_t idx, float cc);

  /**
   * Open the I2C communication
   * @param Optional address for the I2C slave. Default is RelianceADC::DEFAULT_I2C_ADDRESS
   * @return True upon success
   **/
  bool Open(uint32_t i2c_address=0);

  /**
   * Check if the I2C communication is Open
   * @return True if the communication is open
   */
  bool IsOpen();

  /**
   * Close the I2C communication
   **/
  void Close();

  /**
   * Read the ADC given channel with the given gain
   * and scale it to KiloOhms via the calibration constants
   * @param channel The channel to read
   * @param gain The gain of the ADC
   * @return floating point value of the resistance in KiloOhms
   **/
  float ReadKOhm(uint32_t channel, uint32_t gain);

  /**
   * Read the ADC given channel with the given gain
   * @param channel The channel to read
   * @param gain The gain of the ADC
   * @return unsigned 32-bit value of the resistance in ADC counts
   **/
  uint32_t ReadADC(uint32_t channel, uint32_t gain);

  /**
   * Calibrate an adc count to kiloOhms.
   * We asume the same calibration constant for all the channels
   * @param adc ADC counts
   * @gain the gain of the ADC counts
   * @return floating point value of the resistance in KiloOhms
   **/
  float Calibrate(uint32_t adc, uint32_t gain);
  
private:

  /**
   * Create an empty RelianceADC.
   * Private to force users to use the Singleton.
   **/
  RelianceADC();

  /**
   * Write a 16-bit value to a register through I2C.
   * Implemented as a write of size 3 to the file descriptor.
   * The message is {register, LSB, MSB}.
   * @param reg Register address to write to
   * @param value Unsigned 16-bit value to write
   **/
  void Write(uint8_t reg, uint16_t value); 
  
  /**
   * Read a 16-bit value from a register through I2C.
   * Implemented as a write of size 2 from the file descriptor, 
   * followed by a read of size 2 from the file descriptor.
   * The write message is {register}, the read message has the bytes reversed.
   * @param reg Register address to read from
   * @return Unsigned 16-bit value 
   **/
  uint16_t Read(uint8_t reg);

  bool m_verbose; //!< The verbose flag
  int32_t m_fd; //!< File pointer for the I2C communication
  uint32_t m_i2c_address; //!< I2C slave address
  
  std::map<uint32_t, std::vector<float> > m_calib; //!< calibration constants from ADC to Ohms 

  static RelianceADC * m_me; //!< singleton pointer to be returned by RelianceADC::GetInstance

  std::mutex m_lock;

};

#endif
