#ifndef CIRCULAR_BUFFER_H
#define CIRCULAR_BUFFER_H

#include <cstdint>
#include <vector>

/**
 * CircularBuffer is a floating point value container that overwrites itself
 * once it reaches the maximum number of positions allocated.
 * The number of allocated values is provided at construction,
 * and can be changed by CircularBuffer::SetSize.
 * Existing values are zeroed if the size is changed.
 * New values are added to the buffer with CircularBuffer::Add,
 * and the previous value is returned.
 * The average of the values in the buffer (CircularBuffer::GetAverage)
 * is calculated each time a new value is added (CircularBuffer::Add).
 *
 * @brief A Circular buffer to calculate averages on the fly
 * @author Carlos.Solans@cern.ch
 * @date September 2020
 */
class CircularBuffer {
public:

  /**
   * Create a new CircularBuffer with a given size
   * @param size The number positions allocated
   */
  CircularBuffer(uint32_t size);

  /**
   * Clear the CircularBuffer
   */
  ~CircularBuffer();

  /**
   * Clear the CircularBuffer
   */
  void Clear();

  /**
   * Add a new value to the CircularBuffer
   * @param value The value added to the container
   * @return The value removed from the container
   */
  float Add(float value);

  /**
   * Change the size of the CircularBuffer
   * @param size The new size for the container
   */
  void SetSize(uint32_t size);

  /**
   * Get the size of the CircularBuffer
   * @return The new size of the container
   */
  uint32_t GetSize();

  /**
   * Get the average of the values in the CircularBuffer.
	 * By default a quick average is returned from the sum of the values calculated 
	 * in CircularBuffer::Add divided by the size.
   * @param force Force the calculation of the average by summing the values again
	 * @return The average value (arithmetic mean) of the values in the container
   */
  float GetAverage(bool force=false);

private:

  uint32_t m_pos;
  uint32_t m_size;
	float m_sum;

  std::vector<float> m_data;

};

#endif
