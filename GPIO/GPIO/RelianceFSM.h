#ifndef RELIANCEFSM_H
#define RELIANCEFSM_H

#include "RelianceADC.h"
#include "CircularBuffer.h"
#include "RelianceLED.h"
#include "RelianceOutput.h"
#include "Fifo.h"
#include <stdint.h>
#include <vector>
#include <thread>

/**
 * This class reads out the Reliance ADC through I2C
 * converts to resistance via calibration constants,
 * calculates the chasing averages with the help of the CircularBuffer,
 * and checks the alarm state by comparing the chasing averages to a threshold.
 *
 * The class implements an internal FSM (CONFIG, TRAINING, READY, ERROR),
 * where the initial state is CONFIG that takes one read-out cycle,
 * after which it enters the TRAINING state.
 * This state is allows the chasing averages to be defined.
 * After (C+D) cycles, the state changes to READY.
 * Any change to the settings will reset the FSM and bring it to CONFIG,
 * by triggering a RESET command.
 * Any unexpected error in the code will bring the FSM to ERROR,
 * after which the FSM needs to be manually RESET.
 * No alarm or chasing average will be reported if not in READY state.
 *
 * The chasing average is implemented as the average of the first D (duck) samples
 * divided by the average of the second C (crocodile) samples.
 * This magnitude evaluates the derivative of the signal taking into account the
 * mean value of the expected signal in absence of water.
 *
 * @brief The read-out of the RELIANCE box
 * @author florian.haslbeck@cern.ch
 * @author carlos.solans@cern.ch
 * @author ignacio.asensi@cern.ch
 * @date June 2020 - Modified September 2020
 * @date August 2021 : Migrate to RelianceADC
 * @data September 2021 : Remove the singleton from this class
 * @data October 2023 : Add DSS outputs
 */


class RelianceFSM {

public:

  static const uint32_t STATE_CONFIG=0;   /**< Server is configuring */
  static const uint32_t STATE_TRAINING=1; /**< Server is training */
  static const uint32_t STATE_READY=2;    /**< Server is ready */
  static const uint32_t STATE_ERROR=3;    /**< Server is in error */

  static const uint32_t SINGLE_ENDED=0;   /**< ADC mode : single ended */
  static const uint32_t DIFFERENTIAL=1;   /**< ADC mode : differential mode */

  /**
   * @brief Constructor.
   * @param mode ADC mode
   * @param gain ADC gain
   * @param ch   ADC single ended channel
   **/
  RelianceFSM(uint32_t mode = SINGLE_ENDED , uint32_t gain = 16, uint32_t ch = 0);

  /**
   * @brief Destructor.
   **/
  ~RelianceFSM();

  /**
   * Get the verbose mode
   * @return True if the verbose mode is enabled
   */
  bool GetVerbose();

  /**
   * @brief Get FSM operational state.
   * @return state
   **/
  uint32_t GetState();

  /**
   * @brief Get the alarm status.
   * @return true if alarm else false
   **/
  bool GetAlarm();

  /**
   * @brief Get IC2 address.
   * @return address
   **/
  uint32_t GetI2CAddress();

  /**
   * @brief Get ADC channel. Only used in single ended mode.
   * @return channel
   **/
  uint32_t GetAdcChan();

  /**
   * @brief Get ADC reading mode.
   * @return ADC reading mode. 0 = SINGLE_ENDED, 1 = DIFFERENTIAL
   **/
  uint32_t GetAdcMode();

  /**
   * @brief Get ADC gain.
   * @return gain 23 = +/-6.144V,
   *               1 = +/-4.096V,
   *               2 = +/-2.048V,
   *               4 = +/-1.024V,
   *               8 = +/-0.512V,
   *              16 = +/-0.256V
   **/
  uint32_t GetAdcGain();

  /**
   * @brief Get minimum bound of ADC range.
   * @return minimum allowed ADC value
   **/
  uint32_t GetMinAdc();

  /**
   * @brief Get maximum bound of ADC range.
   * @return maximum allowed ADC value
   **/
  uint32_t GetMaxAdc();

  /**
   * @brief Get sleep in between ADC readings.
   * @return The sleep time in between readings in microseconds
   **/
  uint32_t GetPeriod();

  /**
   * @brief Get the maximum number of failed readings before entering the ERROR state.
   * @return The sleep maximum number of failed readings before entering the ERROR state.
   **/
  uint32_t GetMaxErr();

  /**
   * @brief Get sleep before retry reading if last ADC value was outside ADC range.
   * @return sleep in between correct ADC readings in microseconds
   **/
  uint32_t GetErrorSleep();

  /**
   * @brief Get sample size for rolling average C.
   * @return sample size C
   **/
  uint32_t GetBufferLenC();

  /**
   * @brief Get sample size for rolling average D. 
   * @return sample size D
   **/
  uint32_t GetBufferLenD();

  /**
   * @brief Get the alarm threshold
   * @return The alarm threshold 
   **/
  float GetThreshold();

  /**
   * Calibration constants are used to tranform the ADC counts into resistance
   * using a polynomial function. The constants are the factors of each term.
   * f(x,p0,p1,p2,...) = p0*x^0 + p1*x^2 + p2*x^2 + ...
   * @brief Get the calibration constant for a given index
   * @param idx The degree of the polynomial: idx=0 means order 0
   **/
  float GetCalib(uint32_t idx);

  /**
   * @brief Get maximum fifo depth.
   * @return maximum fifo depth
   */
  uint32_t GetFifoSize();

  /**
   * Set the verbose mode
   * @param enable Enable the verbose mode if True
   */
  void SetVerbose(bool enable);

  /**
   * @brief Set IC2 address.
   * @param address IC2 Address. 
   **/
  void SetI2CAddress(uint32_t address = 0x48);

  /**
   * @brief Select ADC reading mode.
   * @param mode ADC reading mode - SINGLE_ENDED (default) or DIFFERENTIAL
   **/
  void SetAdcMode(uint32_t mode = SINGLE_ENDED);

  /**
   * @brief Select ADC gain.
   * @param gain: 23 = +/-6.144V,
   *               1 = +/-4.096V,
   *               2 = +/-2.048V,
   *               4 = +/-1.024V,
   *               8 = +/-0.512V,
   *   (default)  16 = +/-0.256V
   **/
  void SetAdcGain(uint32_t gain = 16);

  /**
   * @brief Select ADC channel. Only used in single ended mode.
   * @param ch ADC channel. 0,1,2,3
   **/
  void SetAdcChan(uint32_t ch = 0);

  /**
   * @brief Set minimum bound of ADC range. If ADC reading is outside of range, Read() will set STATUS_FAULT.
   * @param val minimum allowed ADC value
   **/
  void SetMinAdc(uint32_t val);

  /**
   * @brief Set maximum bound of ADC range. If ADC reading is outside of range, Read() will set STATUS_FAULT.
   * @param val maximum allowed ADC value
   **/
  void SetMaxAdc(uint32_t val);

  /**
   * @brief Set maximum bound of ADC range. If ADC reading is outside of range, Read() will set STATUS_FAULT.
   * @param val maximum allowed ADC value
   **/
  void SetMaxErr(uint32_t val);

  /**
   * @brief Set sleep in between correct ADC readings. Uses usleep() from unistd.h.
   * @param val sleep in microseconds
   **/
  void SetPeriod(uint32_t val);

  /**
   * @brief Set sample size for rolling average C, and change state to STATE_CONFIG.
   * @param val sample size C
   **/
  void SetBufferLenC(uint32_t val);

  /**
   * @brief Set sample size for rolling average D, and change state to STATE_CONFIG.
   * @param val sample size D
   **/
  void SetBufferLenD(uint32_t val);

  /**
   * Set the threshold for triggering the alarm condition, and change state to STATE_CONFIG.
   * If the chasing average is higher than the threshold, alarm is set to true.
   * @brief Set the alarm threshold, and change state to STATE_CONFIG.
   * @param val The threshold value as a floating point number
   **/
  void SetThreshold(float val);

  /**
   * The resistance is calculated by applying a polynomial function to the ADC.
   * The coefficients for each degree of the polynomial function can be changed with this method.
   * f(x,p0,p1,p2,...) = p0*x^0 + p1*x^2 + p2*x^2 + ...
   * @brief Change the calibration constant for a given degree of a polynomial function used to convert the ADC to kOhm.
   * @param idx The degree of the polynomial: idx=0 means order 0
   * @param cc The value of the calibration constant
   */
  void SetCalib(uint32_t idx, float cc);

  /**
   * @brief Set maximum fifo depth. If maximum depth is reached, oldest value pair (timestamp, trigger condition) is popped.
   * @param val maximum fifo depth
   **/
  void SetFifoSize(uint32_t val);

  /**
   * Change state to STATE_CONFIG.
   * @brief Reset the internal FSM to CONFIG
   **/
  void Reset();

  /**
   * @brief Reset the alarm flag without changing the state
   **/
  void ResetAlarm();

  /**
   * @brief Read the last time stamp value
   * @return UNIX time stamp without locale
   */
  uint32_t ReadTimeStamp();

  /**
   * @brief Read the last ADC value
   * @return The ADC value
   */
  uint16_t ReadAdc();

  /**
   * @brief Read the last resistance value (Ohm)
   * @return The resistance value in Ohms
   */
  float ReadResistance();

  /**
   * @brief Read the last chasing averages value
   * @return The chasing average value
   */
  float ReadChasingAverage();

  /**
   * Each value in the FIFO contains an ADC value preceded by a time stamp.
   * To read one ADC value it is necessary to read the FIFO twice.
   * The value read the first time is the time stamp, and second time is the ADC value.
   * @brief Read the ADC FIFO
   * @return the time stamp followed by the ADC value
   */
  uint32_t ReadFifoAdc();

  /**
   * Each value in the FIFO contains the resistance in Ohms preceded by a time stamp.
   * To read one resistance value it is necessary to read the FIFO twice.
   * The value read the first time is the time stamp, and second time is the resistance value.
   * @brief Read the resistance FIFO
   * @return the time stamp followed by the resistance value in Ohms
   */
  uint32_t ReadFifoResistance();

  /**
   * Each value in the FIFO contains the value of the chasing averages multiplied by 1000 preceded by a time stamp.
   * To read one chasing average value it is necessary to read the FIFO twice.
   * The value read the first time is the time stamp, and second time is the chasing average value.
   * @brief Read the chasing averages FIFO
   * @return the time stamp followed by the chasing averages multiplied by 1000
   */
  uint32_t ReadFifoChasingAverage();


private:

  /**
   * @brief Read sample resistance, calculate and check trigger.
   * Push last time stamp and last trigger condition to a FIFO.
   **/
  void Run();

  bool m_verbose;
  uint32_t m_state;
  bool m_alarm;

  std::thread m_thread;
  bool m_running;

  RelianceADC * m_ads;
  RelianceLED * m_leds;
  uint32_t m_i2c_address;
  uint32_t m_mode;
  uint32_t m_gain;
  uint32_t m_ch;

  std::vector<float> m_calib; /**< calibration constants from ADC to Ohms **/

  uint32_t m_min_adc;
  uint32_t m_max_adc;
  uint32_t m_max_err; // # failed reading => STATUS_FAULT
  uint32_t m_sleep;   // [usec]

  uint32_t m_len_c;
  uint32_t m_len_d;
  CircularBuffer * m_ca_d;
  CircularBuffer * m_ca_c;

  float m_threshold;

  uint32_t m_fifo_size;
  Fifo * m_fifo_adc;
  Fifo * m_fifo_kohm;
  Fifo * m_fifo_ca;

  uint32_t m_time;
  uint32_t m_adc;
  float m_res;
  float m_ca;

 };

#endif
