#ifndef RELIANCEBOX_H
#define RELIANCEBOX_H

#include <stdint.h>
#include <map>

class RelianceFSM;

/**
 *
 * @brief A box containing multiple RelianceFSM objects
 * @author carlos.solans@cern.ch
 * @date September 2021 
 */

class RelianceBox {

public:

  /**
   * Delete the RelianceFSM objects stored in memory
   **/
  ~RelianceBox();
  
  /**
   * This is the way to get the RelianceBox object
   **/
  static RelianceBox * GetInstance();

  /**
   * Get a RelianceFSM
   * @param channel the input channel of the FSM
   * @retur the RelianceFSM corresponding to channel
   **/
  RelianceFSM * GetFSM(uint32_t channel);

  /**
   * Store a RelianceFSM
   * @param channel the input channel of the FSM
   * @param fsm the RelianceFSM to store
   **/
  void SetFSM(uint32_t channel, RelianceFSM* fsm);
  
  /**
   * Calculate the LED color
   **/
  void CalculateLED();
  /**
   * Calculate the Outputs status
   **/
  void CalculateOutputs();

  /**
   * Signal the system is starting
   **/
  void Startup();

  /**
   * Signal the system is shutting down
   **/
  void Shutdown();

  /**
   * Get output 
   * @param ch channel 
  */
  bool GetOutput(uint32_t ch);
  
private:

  /**
   * @brief Constructor.
   **/
  RelianceBox();

  /**
   * The internal storage of RelianceFSM.
   * Created upon request by the user with RelianceBox::GetFSM
   * if the FSM does not exist for the corresponding channel.
   * Or stored by the user with RelianceBox::SetFSM.
   * The objects stored are owned by the class, 
   * and Will be deleted by the destructor.
   **/
  std::map<uint32_t,RelianceFSM*> m_fsms;

  /**
   * Only alow one box per process
   **/
  static RelianceBox * m_me;

};

#endif //RELIANCEBOX_H
