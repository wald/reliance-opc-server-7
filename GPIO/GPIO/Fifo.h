#ifndef FIFO_H
#define FIFO_H

#include <cstdint>
#include <queue>

/**
 *
 * @brief FIFO
 * @author Carlos.Solans@cern.ch
 * @date September 2020
 */
class Fifo {
public:

  /**
   */
  Fifo(uint32_t size);

  /**
   */
  ~Fifo();

  /**
   */
  void SetVerbose(bool enable);

  /**
   */
  void Clear();

  /**
   */
  void Write(uint32_t value);

  /**
   */
  uint32_t Read();

  /**
   */
  void SetSize(uint32_t size);

  /**
   */
  uint32_t GetSize();


private:

  bool m_verbose;

  uint32_t m_size;

  std::queue<uint32_t> m_data;

};

#endif
