#ifndef RELIANCELED_H
#define RELIANCELED_H

#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>

/**
 *
 * @brief FIFO
 * @author Ignacio.Asensi@cern.ch
 * @date November 2022
 */
class RelianceLED {
public:

  /**
   */
  RelianceLED(const char *pFile, int flags = O_RDWR);
  /**
   */
  ~LinuxFile();
  size_t Write(const void *pBuffer, size_t size);
  size_t Read(void *pBuffer, size_t size);
  size_t Write(const char *pText);
  size_t Write(int number);
    

private:
  int m_Handle;
  
};

#endif
