#ifndef RELIANCEOUTPUT_H
#define RELIANCEOUTPUT_H

#include <stdint.h>
#include <map>
#include <vector>
#include <mutex>
#include <unistd.h>

/**
 *
 * @brief OUTPUT control for Reliance box
 * @author ignacio.asensi@cern.ch
 * @date October 2023
 **/

class RelianceOutput {

public:

  static const uint32_t OUTPUT_1_ADD = 21;
  static const uint32_t OUTPUT_2_ADD = 22;
  static const uint32_t OUTPUT_3_ADD = 23;
  static const uint32_t OUTPUT_4_ADD = 24;

  static const uint32_t MODE_TRIGGERED=0;
  static const uint32_t MODE_NOTTRIGGERED=1;
  

  /**
   * Close the communication if any
   **/
  ~RelianceOutput();

  /**
   * Get the single instance
   **/
  static RelianceOutput * GetInstance();

  /**
   * Get the verbose mode
   * @return True if the verbose mode is enabled
   */
  bool GetVerbose();

  /**
   * Set the verbose mode
   * @param enable Enable the verbose mode if True
   */
  void SetVerbose(bool enable);

  /**
   * Change the state of the OUTPUT
   * @param wpi wiringPi number
   * @param enable turn on if true
   **/
  void Write(uint32_t wpi, bool enable);

  /**
   * Change the state of the OUTPUT
   * @param wpi wiringPi number
   * @return true if enabled
   **/
  bool Read(uint32_t wpi);

  /**
   * Turn On one OUTPUT
   * @param output OUTPUT to turn on
   **/
  void SetTriggered(uint32_t output);

  /**
   * Turn Off one OUTPUT
   * @param output OUTPUT to turn off
   **/
  void SetNotTriggered(uint32_t output);

  bool GetOutput(uint32_t ch);
 private:

  /**
   * Create an empty RelianceOutput.
   * Private to force users to use the Singleton.
   **/
  RelianceOutput();

  bool Open();

  void Close();

  void SetMode(uint32_t wpi, uint32_t mode);

  uint32_t m_pi_base;

  uint32_t m_gpio_base;

  std::vector<int8_t> m_pin_to_gpio;

  std::vector<uint8_t> m_gpio_to_set;

  std::vector<uint8_t> m_gpio_to_clr;

  std::vector <uint8_t> m_gpio_to_lev;

  std::vector <uint8_t> m_gpio_to_sel;

  std::vector <uint8_t> m_gpio_to_shift;


  bool OUTPUTS[5];
  uint32_t * m_gpio;

  bool m_verbose;

  int32_t m_fd;

  static RelianceOutput * m_me; //!< singleton pointer to be returned by RelianceOUTPUT::GetInstance

  std::mutex m_lock;

};

#endif
