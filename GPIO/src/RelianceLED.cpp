#include "GPIO/RelianceLED.h"

#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <iostream>
#include <sys/mman.h>
using namespace std;

RelianceLED * RelianceLED::m_me=0;

RelianceLED::RelianceLED(){
  m_verbose=0;
  m_fd=-1;
  m_lock.unlock();

  uint32_t model=2711;
  
  m_pi_base=0x20000000;
  if      (model==2835){ m_pi_base=0x3F000000;} 
  else if (model==2711){ m_pi_base=0xFE000000;}

  m_gpio_base = m_pi_base + 0x00200000;
  
  m_pin_to_gpio = {
    17, 18, 27, 22, 23, 24, 25, 4,// From the Original Wiki - GPIO 0 through 7:wpi  0 -  7
    2,  3,// I2C  - SDA0, SCL0wpi  8 -  9
    8,  7,// SPI  - CE1, CE0wpi 10 - 11
    10,  9, 11, // SPI  - MOSI, MISO, SCLKwpi 12 - 14
    14, 15,// UART - Tx, Rxwpi 15 - 16
    28, 29, 30, 31,// Rev 2: New GPIOs 8 though 11wpi 17 - 20
    5,  6, 13, 19, 26,// B+wpi 21, 22, 23, 24, 25
    12, 16, 20, 21,// B+wpi 26, 27, 28, 29
    0,  1,// B+wpi 30, 31
    // Padding:
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,// ... 47
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,// ... 63
  } ;

  m_gpio_to_set = {
    7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
    8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
  };

  m_gpio_to_clr = {
    10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,
    11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
  };


  m_gpio_to_lev =
    {
     13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,
     14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,
    };

  m_gpio_to_sel =
    {
     0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,
     3,3,3,3,3,3,3,3,3,3,4,4,4,4,4,4,4,4,4,4,5,5,5,5,5,5,5,5,5,5,
    };

  m_gpio_to_shift =
    {
     0,3,6,9,12,15,18,21,24,27,0,3,6,9,12,15,18,21,24,27,0,3,6,9,12,15,18,21,24,27,
     0,3,6,9,12,15,18,21,24,27,0,3,6,9,12,15,18,21,24,27,0,3,6,9,12,15,18,21,24,27,
    } ;
		    
  Open();

  SetMode(LED_RED,MODE_OUTPUT);
  SetMode(LED_YELLOW,MODE_OUTPUT);
  SetMode(LED_GREEN,MODE_OUTPUT);
  
}

RelianceLED::~RelianceLED(){
  Close();
}

RelianceLED * RelianceLED::GetInstance(){
  if(m_me==0){ m_me = new RelianceLED();}
  return m_me;
}

bool RelianceLED::Open(){
  m_lock.lock();
  m_fd=open("/dev/gpiomem", O_RDWR | O_SYNC | O_CLOEXEC);
  if(m_fd<0){cout << "Failed to open: /dev/gpiomem" << endl; return false;}
  m_gpio = (uint32_t *)mmap(0, (4*1024), PROT_READ|PROT_WRITE, MAP_SHARED, m_fd, m_gpio_base);
  if(m_gpio==MAP_FAILED) {cout << "Cannot mamp /dev/gpiomem" << endl;}
  m_lock.unlock();
  return true;
}

void RelianceLED::Close(){

}

void RelianceLED::Write(uint32_t wpi, bool enable){
  if(m_verbose){cout << "RelianceLED::Write (wpi:" << wpi << ", enable: " << enable << ")" << endl;}
  int32_t gpio = m_pin_to_gpio[wpi];
  if(m_verbose){cout << "wpi: " << wpi << ", gpio: " << gpio << endl;}
  if(gpio<0) return;
  if(enable){
    *(m_gpio + m_gpio_to_set[gpio]) = 1 << gpio;
  }else{
    *(m_gpio + m_gpio_to_clr[gpio]) = 1 << gpio;
  }
}

bool RelianceLED::Read(uint32_t wpi){
  if(m_verbose){cout << "RelianceLED::Read (wpi:" << wpi << ")" << endl;}
  int32_t gpio = m_pin_to_gpio[wpi];
  if(m_verbose){cout << "wpi: " << wpi << ", gpio: " << gpio << endl;}
  if(gpio<0) return false;
  return (((*(m_gpio + m_gpio_to_lev[gpio])) & (1 << gpio)) != 0); 
}

void RelianceLED::SetMode(uint32_t wpi, uint32_t mode){
  if(m_verbose){cout << "RelianceLED::SetMode (wpi:" << wpi << ", mode: " << mode << ")" << endl;}
  int32_t gpio = m_pin_to_gpio[wpi];
  uint8_t fSel = m_gpio_to_sel[gpio];
  uint8_t shift= m_gpio_to_shift[gpio];
 
  if (mode == MODE_INPUT){
    *(m_gpio + fSel) = (*(m_gpio + fSel) & ~(7 << shift)) ; // Sets bits to zero = input
  }else if (mode == MODE_OUTPUT){
    *(m_gpio + fSel) = (*(m_gpio + fSel) & ~(7 << shift)) | (1 << shift) ;
  }
}

void RelianceLED::SetVerbose(bool enable){
  m_verbose=enable;
}

void RelianceLED::TurnOn(uint32_t led){
 if(led!=LED_RED and led!=LED_YELLOW and led!=LED_GREEN){return;}
 Write(led,true);
}

void RelianceLED::TurnOff(uint32_t led){
 if(led!=LED_RED and led!=LED_YELLOW and led!=LED_GREEN){return;}
 Write(led,false);
}
