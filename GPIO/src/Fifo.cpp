#include "GPIO/Fifo.h"
#include <cmath>
#include <iostream>

using namespace std;

Fifo::Fifo(uint32_t size){
  m_size=size;
  m_verbose=false;
}

Fifo::~Fifo(){
  Clear();
}

void Fifo::SetVerbose(bool enable){
  m_verbose=enable;
}

void Fifo::Clear(){
  while(!m_data.empty()){m_data.pop();}
}

void Fifo::Write(uint32_t value){
  if(m_verbose){cout << "Write " << value << endl;}
  if(m_data.size()==m_size){
    m_data.pop();
  }
  m_data.push(value);
}

uint32_t Fifo::Read(){
  uint32_t value=m_data.front();
  m_data.pop();
  if(m_verbose){cout << "Read  " << value << endl;}
  return value;
}

void Fifo::SetSize(uint32_t size){
  m_size=size;
}

uint32_t Fifo::GetSize(){
  return m_size;
}

