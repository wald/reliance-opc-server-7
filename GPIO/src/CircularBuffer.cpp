#include "GPIO/CircularBuffer.h"

using namespace std;

CircularBuffer::CircularBuffer(uint32_t size){
  m_data.resize(size,0);
  m_size=size;
	m_sum=0;
}

CircularBuffer::~CircularBuffer(){
  m_data.clear();
}

float CircularBuffer::Add(float value){
  float prev=m_data[m_pos];
  m_data[m_pos]=value;
	m_sum += (value-prev);
  m_pos++;
  if(m_pos==m_size){m_pos=0;}
  return prev;
}

void CircularBuffer::Clear(){
  m_data.clear();
  m_data.resize(m_size,0);
  m_pos=0;
	m_sum=0;
}

void CircularBuffer::SetSize(uint32_t size){
  m_size=size;
  Clear();
}

uint32_t CircularBuffer::GetSize(){
  return m_size;
}

float CircularBuffer::GetAverage(bool force){
	if(force){
		m_sum = 0.;
		for (uint32_t i=0;i<m_size;i++){	m_sum += m_data[i]; }
	}
	return m_sum/(float)m_size;
}
