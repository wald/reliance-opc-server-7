#include "GPIO/RelianceBox.h"
#include "GPIO/RelianceFSM.h"
#include "GPIO/RelianceLED.h"
#include "GPIO/RelianceOutput.h"
#include <iostream>
using namespace std;

RelianceBox * RelianceBox::m_me=0;

RelianceBox::RelianceBox(){}

RelianceBox::~RelianceBox(){
  for(auto& it: m_fsms){ delete it.second; }
  m_fsms.clear();
}

RelianceBox * RelianceBox::GetInstance(){
  if(m_me==0){m_me=new RelianceBox();}
  return m_me;
}

RelianceFSM * RelianceBox::GetFSM(uint32_t chan){
  if(m_fsms[chan]==NULL){m_fsms[chan]=new RelianceFSM();}
  return m_fsms[chan];
}

void RelianceBox::SetFSM(uint32_t chan, RelianceFSM * fsm){
  m_fsms[chan]=fsm;
}

void RelianceBox::CalculateLED(){

  bool yellow=false;
  bool red=false;
  for(auto &it : m_fsms){
    if(it.second->GetAlarm()){yellow=true;}
    if(it.second->GetState()==RelianceFSM::STATE_CONFIG){red=true;}
    else if(it.second->GetState()==RelianceFSM::STATE_TRAINING){red=true;}
    else if(it.second->GetState()==RelianceFSM::STATE_ERROR){red=true;}
  }
  if(red and !yellow){
    RelianceLED::GetInstance()->Write(RelianceLED::LED_GREEN, false);
    RelianceLED::GetInstance()->Write(RelianceLED::LED_YELLOW, false);
    RelianceLED::GetInstance()->Write(RelianceLED::LED_RED, true);
  }else if(!red and yellow){
    RelianceLED::GetInstance()->Write(RelianceLED::LED_GREEN, false);
    RelianceLED::GetInstance()->Write(RelianceLED::LED_YELLOW, true);
    RelianceLED::GetInstance()->Write(RelianceLED::LED_RED, false);
  }else if(!red and !yellow){
    RelianceLED::GetInstance()->Write(RelianceLED::LED_GREEN, true);
    RelianceLED::GetInstance()->Write(RelianceLED::LED_YELLOW, false);
    RelianceLED::GetInstance()->Write(RelianceLED::LED_RED, false);
  }

}
bool RelianceBox::GetOutput(uint32_t ch){
  return RelianceOutput::GetInstance()->GetOutput(ch);
}

void RelianceBox::CalculateOutputs(){
    //cout << "CalculateOutputs" ;
    bool trigger=RelianceOutput::MODE_NOTTRIGGERED;
    uint32_t ch=5;
    for(auto &it : m_fsms){
        ch=it.second->GetAdcChan();
        trigger=RelianceOutput::MODE_NOTTRIGGERED;
        if(it.second->GetAlarm()){
            trigger=RelianceOutput::MODE_TRIGGERED;
        }
        if (ch==0){
            RelianceOutput::GetInstance()->Write(RelianceOutput::OUTPUT_1_ADD, trigger);
        }else if (ch == 1){
            RelianceOutput::GetInstance()->Write(RelianceOutput::OUTPUT_2_ADD, trigger);
        }else if (ch == 2){
            RelianceOutput::GetInstance()->Write(RelianceOutput::OUTPUT_3_ADD, trigger);
        }else if (ch == 3){
            RelianceOutput::GetInstance()->Write(RelianceOutput::OUTPUT_4_ADD, trigger);
        }
    }
}

void RelianceBox::Startup(){
  RelianceLED::GetInstance()->Write(RelianceLED::LED_GREEN, true);
  RelianceLED::GetInstance()->Write(RelianceLED::LED_YELLOW, true);
  RelianceLED::GetInstance()->Write(RelianceLED::LED_RED, true);

  RelianceOutput::GetInstance()->Write(RelianceOutput::OUTPUT_1_ADD, RelianceOutput::MODE_NOTTRIGGERED);
  RelianceOutput::GetInstance()->Write(RelianceOutput::OUTPUT_2_ADD, RelianceOutput::MODE_NOTTRIGGERED);
  RelianceOutput::GetInstance()->Write(RelianceOutput::OUTPUT_3_ADD, RelianceOutput::MODE_NOTTRIGGERED);
  RelianceOutput::GetInstance()->Write(RelianceOutput::OUTPUT_4_ADD, RelianceOutput::MODE_NOTTRIGGERED);

}

void RelianceBox::Shutdown(){
  RelianceLED::GetInstance()->Write(RelianceLED::LED_GREEN, false);
  RelianceLED::GetInstance()->Write(RelianceLED::LED_YELLOW, false);
  RelianceLED::GetInstance()->Write(RelianceLED::LED_RED, true);  
}
