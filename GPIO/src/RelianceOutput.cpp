#include "GPIO/RelianceOutput.h"

#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <iostream>
#include <sys/mman.h>
using namespace std;

RelianceOutput * RelianceOutput::m_me=0;

RelianceOutput::RelianceOutput(){
  m_verbose=0;
  m_fd=-1;
  m_lock.unlock();

  uint32_t model=2711;

  m_pi_base=0x20000000;
  if      (model==2835){ m_pi_base=0x3F000000;}
  else if (model==2711){ m_pi_base=0xFE000000;}

  m_gpio_base = m_pi_base + 0x00200000;

  m_pin_to_gpio = {
    17, 18, 27, 22, 23, 24, 25, 4,// From the Original Wiki - GPIO 0 through 7:wpi  0 -  7
    2,  3,// I2C  - SDA0, SCL0wpi  8 -  9
    8,  7,// SPI  - CE1, CE0wpi 10 - 11
    10,  9, 11, // SPI  - MOSI, MISO, SCLKwpi 12 - 14
    14, 15,// UART - Tx, Rxwpi 15 - 16
    28, 29, 30, 31,// Rev 2: New GPIOs 8 though 11wpi 17 - 20
    5,  6, 13, 19, 26,// B+wpi 21, 22, 23, 24, 25
    12, 16, 20, 21,// B+wpi 26, 27, 28, 29
    0,  1,// B+wpi 30, 31
    // Padding:
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,// ... 47
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,// ... 63
  } ;

  m_gpio_to_set = {
    7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
    8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
  };

  m_gpio_to_clr = {
    10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,
    11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
  };


  m_gpio_to_lev =
    {
     13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,
     14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,
    };

  m_gpio_to_sel =
    {
     0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,
     3,3,3,3,3,3,3,3,3,3,4,4,4,4,4,4,4,4,4,4,5,5,5,5,5,5,5,5,5,5,
    };

  m_gpio_to_shift =
    {
     0,3,6,9,12,15,18,21,24,27,0,3,6,9,12,15,18,21,24,27,0,3,6,9,12,15,18,21,24,27,
     0,3,6,9,12,15,18,21,24,27,0,3,6,9,12,15,18,21,24,27,0,3,6,9,12,15,18,21,24,27,
    } ;

  Open();

  SetMode(OUTPUT_1_ADD,MODE_NOTTRIGGERED);
  SetMode(OUTPUT_2_ADD,MODE_NOTTRIGGERED);
  SetMode(OUTPUT_3_ADD,MODE_NOTTRIGGERED);
  SetMode(OUTPUT_4_ADD,MODE_NOTTRIGGERED);
  OUTPUTS[0] = MODE_NOTTRIGGERED;
  OUTPUTS[1] = MODE_NOTTRIGGERED;
  OUTPUTS[2] = MODE_NOTTRIGGERED;
  OUTPUTS[3] = MODE_NOTTRIGGERED;

}

RelianceOutput::~RelianceOutput(){
  Close();
}

RelianceOutput * RelianceOutput::GetInstance(){
  if(m_me==0){ m_me = new RelianceOutput();}
  return m_me;
}

bool RelianceOutput::Open(){
  m_lock.lock();
  m_fd=open("/dev/gpiomem", O_RDWR | O_SYNC | O_CLOEXEC);
  if(m_fd<0){cout << "Failed to open: /dev/gpiomem" << endl; return false;}
  m_gpio = (uint32_t *)mmap(0, (4*1024), PROT_READ|PROT_WRITE, MAP_SHARED, m_fd, m_gpio_base);
  if(m_gpio==MAP_FAILED) {cout << "Cannot mamp /dev/gpiomem" << endl;}
  m_lock.unlock();
  return true;
}

void RelianceOutput::Close(){

}

void RelianceOutput::Write(uint32_t wpi, bool enable){
  //cout << "RelianceOutput::Write (wpi:" << wpi << ", enable: " << enable << ")" << endl;
  int32_t gpio = m_pin_to_gpio[wpi];
  //cout << "wpi: " << wpi << ", gpio: " << gpio << endl;
  if(gpio<0) return;
  if(enable){
    *(m_gpio + m_gpio_to_set[gpio]) = 1 << gpio;
    if(m_verbose){cout << "MODE_NOT TRIGGERED (enable)";}
     switch(wpi){
      case OUTPUT_1_ADD: OUTPUTS[0]=MODE_NOTTRIGGERED;break;
      case OUTPUT_2_ADD: OUTPUTS[1]=MODE_NOTTRIGGERED;break;
      case OUTPUT_3_ADD: OUTPUTS[2]=MODE_NOTTRIGGERED;break;
      case OUTPUT_4_ADD: OUTPUTS[3]=MODE_NOTTRIGGERED;break;
      default: cout << "MODE_NOT TRIGGERED " ;
    }
  }else{
    *(m_gpio + m_gpio_to_clr[gpio]) = 1 << gpio;
    if(m_verbose){cout << "MODE TRIGGERED (not enable)";}
     switch(wpi){
      case OUTPUT_1_ADD: OUTPUTS[0]=MODE_TRIGGERED;break;
      case OUTPUT_2_ADD: OUTPUTS[1]=MODE_TRIGGERED;break;
      case OUTPUT_3_ADD: OUTPUTS[2]=MODE_TRIGGERED;break;
      case OUTPUT_4_ADD: OUTPUTS[3]=MODE_TRIGGERED;break;
      default: cout << "Undefined WPI!!!" ;
    }
  }
 
}

bool RelianceOutput::Read(uint32_t wpi){
  if(m_verbose){cout << "RelianceOutput::Read (wpi:" << wpi << ")" << endl;}
  int32_t gpio = m_pin_to_gpio[wpi];
  if(m_verbose){cout << "wpi: " << wpi << ", gpio: " << gpio << endl;}
  if(gpio<0) return false;
  return (((*(m_gpio + m_gpio_to_lev[gpio])) & (1 << gpio)) != 0);
}

void RelianceOutput::SetMode(uint32_t wpi, uint32_t mode){
  if(m_verbose){cout << "RelianceOutput::SetMode (wpi:" << wpi << ", mode: " << mode << ")" << endl;}
  int32_t gpio = m_pin_to_gpio[wpi];
  uint8_t fSel = m_gpio_to_sel[gpio];
  uint8_t shift= m_gpio_to_shift[gpio];

  if (mode == MODE_TRIGGERED){
    *(m_gpio + fSel) = (*(m_gpio + fSel) & ~(7 << shift)) ; // Sets bits to zero = input
  }else if (mode == MODE_NOTTRIGGERED){
    *(m_gpio + fSel) = (*(m_gpio + fSel) & ~(7 << shift)) | (1 << shift) ;
  }
}

void RelianceOutput::SetVerbose(bool enable){
  m_verbose=enable;
}

void RelianceOutput::SetTriggered(uint32_t output){
 Write(output,MODE_TRIGGERED);
}

void RelianceOutput::SetNotTriggered(uint32_t output){
 Write(output,MODE_NOTTRIGGERED);
}

bool RelianceOutput::GetOutput(uint32_t ch){
 return OUTPUTS[ch];
}
