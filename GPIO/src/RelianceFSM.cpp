#include "GPIO/RelianceFSM.h"
#include <ctime>
#include <unistd.h>
#include <cmath>
#include <iostream>
#include <map>

using namespace std;

RelianceFSM::RelianceFSM(uint32_t mode , uint32_t gain, uint32_t ch){

  m_verbose = false;

  /* FSM */
  m_state  = STATE_CONFIG;
  m_alarm  = false;
  
  /* ADC */
  m_ads = RelianceADC::GetInstance();
  m_i2c_address = 0x48;
  m_mode = mode;
  m_gain = gain;
  m_ch = ch;

  /* Reading */
  m_min_adc = 0;
  m_max_adc = 65535;
  m_max_err = 3;
  m_sleep = 1000000;

  /* ADC to kOhm calibration constants */
  m_calib = {4e-8, 5e-4, 0.}; //For gain 2


  /* Chasing averages */
  m_len_d = 10; //100;
  m_len_c = 90; //900.;
  m_ca_c = new CircularBuffer(m_len_c);
  m_ca_d = new CircularBuffer(m_len_d);

  /* Alarm threshold */
  m_threshold = 1.05;

  /* values */
  m_time = 0;
  m_adc = 0;
  m_res = 0.;
  m_ca = 1.;

  /* Fifos */
  m_fifo_size = 10;
  m_fifo_adc  = new Fifo(m_fifo_size);
  m_fifo_kohm = new Fifo(m_fifo_size);
  m_fifo_ca   = new Fifo(m_fifo_size);
  
  m_running = true;
  m_thread = thread(&RelianceFSM::Run,this);
}

RelianceFSM::~RelianceFSM(){
  m_running = false;
  m_thread.join();

  delete m_ca_d;
  delete m_ca_c;

  delete m_fifo_adc;
  delete m_fifo_kohm;
  delete m_fifo_ca;
}

bool RelianceFSM::GetVerbose(){
  return m_verbose;
}

uint32_t RelianceFSM::GetState(){
  return m_state;
}

bool RelianceFSM::GetAlarm(){
  return m_alarm;
}

uint32_t RelianceFSM::GetBufferLenC(){
  return m_len_c;
}

uint32_t RelianceFSM::GetBufferLenD(){
  return m_len_d;
}

float RelianceFSM::GetThreshold(){
  return m_threshold;
}

uint32_t RelianceFSM::GetMaxErr(){
  return m_max_err;
}

uint32_t RelianceFSM::GetPeriod(){
  return m_sleep;
}

float RelianceFSM::GetCalib(uint32_t idx){
  return m_calib[idx];
}

uint32_t RelianceFSM::GetI2CAddress(){
  return m_i2c_address;
}

uint32_t RelianceFSM::GetAdcChan(){
  return m_ch;
}

uint32_t RelianceFSM::GetAdcMode(){
  return m_mode;
}

uint32_t RelianceFSM::GetAdcGain(){
  return m_gain;
}

uint32_t RelianceFSM::GetMinAdc(){
  return m_min_adc;
}

uint32_t RelianceFSM::GetMaxAdc(){
  return m_max_adc;
}

uint32_t RelianceFSM::GetFifoSize(){
  return m_fifo_size;
}

void RelianceFSM::SetVerbose(bool enable){
  m_verbose=enable;
  m_ads->SetVerbose(m_verbose);
}

void RelianceFSM::SetBufferLenC(uint32_t val){
  m_len_c = val;
  m_state = STATE_CONFIG;
}

void RelianceFSM::SetBufferLenD(uint32_t val){
  m_len_d = val;
  m_state = STATE_CONFIG;
}

void RelianceFSM::SetThreshold(float val){
  m_threshold = val;
}

void RelianceFSM::SetMaxErr(uint32_t val){
  m_max_err = val;
}

void RelianceFSM::SetPeriod(uint32_t val){
  m_sleep = val;
}

void RelianceFSM::SetCalib(uint32_t idx, float cc){
  m_calib[idx] = cc;
}

void RelianceFSM::SetI2CAddress(uint32_t address){
  m_i2c_address = address;
  m_state = STATE_CONFIG;
}

void RelianceFSM::SetAdcChan(uint32_t ch){
  m_ch = ch;
}

void RelianceFSM::SetAdcMode(uint32_t mode){
  m_mode = mode;
}

void RelianceFSM::SetAdcGain(uint32_t gain){
  m_gain = gain;
}

void RelianceFSM::SetMinAdc(uint32_t val){
  m_min_adc = val;
}

void RelianceFSM::SetMaxAdc(uint32_t val){
  m_max_adc = val;
}

void RelianceFSM::SetFifoSize(uint32_t size){
  m_fifo_size = size;
  m_fifo_adc->SetSize(size);
  m_fifo_kohm->SetSize(size);
  m_fifo_ca->SetSize(size);
}

void RelianceFSM::Reset(){
  m_state = STATE_CONFIG;
}

void RelianceFSM::ResetAlarm(){
  m_alarm = false;
}

uint32_t RelianceFSM::ReadTimeStamp(){
  return m_time;
}

uint16_t RelianceFSM::ReadAdc(){
  return m_adc;
}

float RelianceFSM::ReadResistance(){
  return m_res;
}

float RelianceFSM::ReadChasingAverage(){
  return m_ca;
}

uint32_t RelianceFSM::ReadFifoAdc(){
  return m_fifo_adc->Read();
}

uint32_t RelianceFSM::ReadFifoResistance(){
  return m_fifo_kohm->Read();
}

uint32_t RelianceFSM::ReadFifoChasingAverage(){
  return m_fifo_ca->Read();
}

void RelianceFSM::Run(){
  uint32_t stp=0;
  uint32_t failed=0;

  while ( m_running ) {

    if(m_ads->GetI2CAddress()!=m_i2c_address){
      if(m_ads->IsOpen()){m_ads->Close();}
    }
    if(!m_ads->IsOpen()){m_ads->Open(m_i2c_address);}
  
    if(m_state == STATE_CONFIG){
      if(m_verbose){cout << "RelianceFSM::Run State Config" << endl;}
      m_ca_c->SetSize(m_len_c);
      m_ca_d->SetSize(m_len_d);
      m_fifo_adc->SetSize(m_fifo_size);
      m_fifo_kohm->SetSize(m_fifo_size);
      m_fifo_ca->SetSize(m_fifo_size);
      m_alarm = false;
      stp=0;
      m_state = STATE_TRAINING;
      continue;
    }
    else if(m_state == STATE_TRAINING){
      if(m_verbose){cout << "RelianceFSM::Run State Training" << endl;}
      stp++;
      if(stp>m_len_c+m_len_d){m_state=STATE_READY;}
    }

    //Get the time stamp
    if(m_verbose){cout << "RelianceFSM::Run Get timestamp" << endl;}
    uint32_t timestamp = (uint32_t) time(0);

    //Read the ADC
    if(m_verbose){cout << "RelianceFSM::Run Read ADC Chan:" << m_ch << " Gain:" << m_gain << endl;}
    static map<uint32_t,uint32_t> gg={{23,0},{1,1},{2,2},{4,3},{8,4},{16,5}};
    uint16_t adc = m_ads->ReadADC(m_ch,gg[m_gain]);
    if(adc>=m_min_adc and adc<=m_max_adc){ failed=0; }
    else{
      failed++;
      if(failed == m_max_err){
        m_state = STATE_ERROR;
      }
      usleep(m_sleep);
      continue;
    }

    //Convert to resistance
    if(m_verbose){cout << "RelianceFSM::Run Convert to resistance" << endl;}
    float kohm = m_ads->Calibrate(adc,gg[m_gain]);

    //Update the circular buffer D with the ADC reading
    //And the circular buffer C with the output of D
    if(m_verbose){cout << "RelianceFSM::Run Add to Circular Buffers" << endl;}
    m_ca_c->Add(m_ca_d->Add(adc));

    //Check alarm status
    if(m_verbose){cout << "RelianceFSM::Run Check alarm" << endl;}
    float ca = 1;
    if(m_ca_c->GetAverage()>0 and m_state==STATE_READY){
      if(m_verbose){cout << "RelianceFSM::Run Duck: " << m_ca_d->GetAverage() << " Crocodile: " << m_ca_c->GetAverage() << endl;}
      ca = m_ca_d->GetAverage()/m_ca_c->GetAverage();
      if (ca>m_threshold){
    	  m_alarm=true;
      }
    }

    if(m_verbose){cout << "RelianceFSM::Run ADC:" << adc << " kOhm:" << kohm << " ca:" << ca << " alarm:" << m_alarm << endl;}

    //Update the last values
    if(m_verbose){cout << "RelianceFSM::Run Update the last values" << endl;}
    m_time = timestamp;
    m_adc = adc;
    m_res = kohm;
    m_ca  = ca;

    //Update the FIFOS
    if(m_verbose){cout << "RelianceFSM::Run Update the FIFOs" << endl;}
    m_fifo_adc ->Write(timestamp);
    m_fifo_adc ->Write(adc);
    m_fifo_kohm->Write(timestamp);
    m_fifo_kohm->Write((uint32_t) (kohm * 1000));
    m_fifo_ca  ->Write(timestamp);
    m_fifo_ca  ->Write((uint32_t) (ca * 1000));

    usleep(m_sleep);
  }
}






