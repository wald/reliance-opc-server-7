#include "GPIO/RelianceADC.h"
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <iostream>
#include <math.h>

using namespace std;

RelianceADC * RelianceADC::m_me=0;

RelianceADC::RelianceADC(){
  m_verbose=0;
  m_fd=-1;
  m_lock.unlock();
  // Calibration constants ADC to kOhm
  // Uku Luhari and Florian Haslbeck, Summer 2023
  m_calib={
	   {GAIN_TWOTHIRDS,{-7e-3,25.4e-4,5.0e-8,2.6e-11}},
	   {GAIN_ONE,      {-1e-3,157.9e-5,14.8e-8,-14.6e-12,9.9e-16}},
	   {GAIN_TWO,      {-8e-3,84.9e-5,5.5e-9,9.8e-13}},
	   {GAIN_FOUR,     {-4e-3,40.8e-5,53.5e-10}},
	   {GAIN_EIGHT,    {-6e-3,205e-6,12.4e-10}},
	   {GAIN_SIXTEEN,  {-10e-3,108e-6}}, 
  };
}

RelianceADC::~RelianceADC(){
  Close();
}

RelianceADC * RelianceADC::GetInstance(){
  if(m_me==0){ m_me = new RelianceADC();}
  return m_me;
}

bool RelianceADC::GetVerbose(){
  return m_verbose;
}

uint32_t RelianceADC::GetI2CAddress(){
  return m_i2c_address;
}

float RelianceADC::GetCalib(uint32_t gain, uint32_t idx){
  return m_calib[gain][idx];
}
  
void RelianceADC::SetVerbose(bool enable){
  m_verbose=enable;
}

void RelianceADC::SetI2CAddress(uint32_t address = 0x48){
  cout << "Setting I2C address: 0x" << hex << address << dec << endl;
  m_i2c_address=address;
}
  
void RelianceADC::SetCalib(uint32_t gain, uint32_t idx, float cc){
  m_lock.lock();
  m_calib[gain][idx]=cc;
  m_lock.unlock();
}

bool RelianceADC::Open(uint32_t i2c_address){
  m_lock.lock();
  if(i2c_address!=0){SetI2CAddress(i2c_address);}
  m_fd=open("/dev/i2c-1", O_RDWR);
  if(m_fd<0){cout << "Failed to open I2C bus: /dev/i2c-1" << endl; return false;}
  ioctl(m_fd, I2C_SLAVE, m_i2c_address);
  cout << "m_i2cFd = " << m_fd << endl;
  m_lock.unlock();
  return true;
}

bool RelianceADC::IsOpen(){
  if(m_verbose){cout << "File descriptor is: " << m_fd << endl;}
  return (m_fd>0?true:false);
}

void RelianceADC::Close(){
  if(m_fd<=0){return;}
  close(m_fd);
  m_fd=-1;
}

float RelianceADC::ReadKOhm(uint32_t channel, uint32_t gain){
  uint32_t adc=ReadADC(channel,gain);
  return Calibrate(adc, gain);
}

float RelianceADC::Calibrate(uint32_t adc, uint32_t gain){
  float kohm=0.0f;
  if(m_calib.find(gain)==m_calib.end()){
    cout << "Calibration constants not found for gain: " << gain << endl;
    return (float)adc;
  }
  for(uint32_t i=0;i<m_calib[gain].size();i++){
    kohm += m_calib[gain][i]*pow(adc,i);
  }
  return kohm;
}

uint32_t RelianceADC::ReadADC(uint32_t channel, uint32_t gain){
  if(channel>3){return 0;}
  if(gain>5){return 0;}
  if(m_fd<0){return 0;}
  //Lock the mutex to avoid concurrent reading
  m_lock.lock();

  //Configure the ADC
  uint16_t config = CONFIG_CQUE_NONE    | // Disable the comparator (default val)
                    CONFIG_CLAT_NOLAT   | // Non-latching (default val)
                    CONFIG_CPOL_ACTLO   | // Alert/Rdy active low   (default val)
                    CONFIG_CMOD_TRAD    | // Traditional comparator (default val)
                    CONFIG_DR_1600SPS   | // 1600 samples per second (default)
                    CONFIG_MODE_SINGLE;   // Single-shot mode (default)

  // Set PGA/voltage range
  switch(gain){
    case 0: config |= CONFIG_PGA_G23; break;
    case 1: config |= CONFIG_PGA_G1; break;
    case 2: config |= CONFIG_PGA_G2; break;
    case 3: config |= CONFIG_PGA_G4; break;
    case 4: config |= CONFIG_PGA_G8; break;
    case 5: config |= CONFIG_PGA_G16; break;
  }

  // Set single-ended input channel
  switch (channel){
    case (0): config |= CONFIG_MUX_SINGLE_0; break;
    case (1): config |= CONFIG_MUX_SINGLE_1; break;
    case (2): config |= CONFIG_MUX_SINGLE_2; break;
    case (3): config |= CONFIG_MUX_SINGLE_3; break;
  }

  // Set 'start single-conversion' bit
  config |= CONFIG_OS_SINGLE;

  // Write config register to the ADC
  Write(POINTER_CONFIG, config);

  // Wait for the conversion to complete
  usleep(1000*ADS1115_CONVERSIONDELAY);

  // Read the conversion results
  // Shift 12-bit results right 4 bits for the ADS1015
  uint32_t adc=Read(POINTER_CONVERT) >> ADS1115_BITSHIFT; 

  //unlock the mutex
  m_lock.unlock();
  return adc;
}

void RelianceADC::Write(uint8_t reg, uint16_t value){
  char msg[3]={(char)reg,(char)(value>>8),(char)(value & 0xFF)};
  write(m_fd,msg,3);
}

uint16_t RelianceADC::Read(uint8_t reg){
  char req[1]={(char)reg};
  write(m_fd,req,1);
  uint8_t rep[2]={0};
  read(m_fd,rep,2);
  return ((rep[0]<<8) + rep[1]);
}
