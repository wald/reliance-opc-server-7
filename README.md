# RELIANCE OPC SERVER

This package is the OPC server for the Reliance water leak detection project that runs on a Raspberry Pi.

## External dependencies

Quasar has the following external dependencies:

- python-lxml 
- cmake 
- libboost1.74 
- libxml2-utils 
- openjdk-17-jdk 
- xsdcxx 
- libxerces-c-dev
- astyle
- libssl-dev 
- kdiff3
- python-colorama
- python-jinja2
- screen 
- nsutils 
- libgit2-dev 
- python-pygit2

Can be installed with a single command

0. Update the repositories
```
sudo apt-get update
```
1. Install the required packages
```
sudo apt-get -y install \
cmake openjdk-17-jdk xsdcxx astyle kdiff3 nsutils \
libboost1.74 libxml2-utils libxerces-c-dev libssl-dev libgit2-dev \ 
python-lxml python-colorama python-jinja2 python-pygit2
```

## How to compile this package

1. Use quasar to build the package
```
./quasar.py build
```

## How to install the opc server as a service

1. Install the service
```
sudo cp reliance-opc-server.service /etc/systemd/system/.
```

2. Enable the opc server
```
sudo systemctl enable reliance-opc-server.service
```

3. Start the server
``` 
sudo systemctl start reliance-opc-server.service
```

## Create a default configuration

1. Copy the file
```
cp bin/raspberrypi.xml bin/$(hostname).xml
```


## How to update the open62541-compat version

1. Disable the module
``` 
./quasar.py disable_module open62541-compat
```

2. Enable the module with a new version: eg v1.4.4
```
./quasar.py enable_module open62541-compat v1.4.4
```

## How to upgrade the version of quasar

1. Checkout the latest version of quasar outside this folder

```
git clone --recursive https://github.com/quasar-team/quasar -b v1.7.0
```

2. Change to the quasar directory
```
cd quasar
```

3. Upgrade the reliance-opc-server
```
./quasar.py upgrade_project ../reliance-opc-server-7
```

4. Change to the reliance project folder
```
  cd ../reliance-opc-server-7
```

5. Regenerate the devices
```
 ./quasar.py generate device --all
```

6. Compile the project
```
 ./quasar.py build
```

