/* © Copyright CERN, Universidad de Oviedo, 2015.  All rights not expressly granted are reserved.
 * QuasarServer.cpp
 *
 *  Created on: Nov 6, 2015
 * 		Author: Damian Abalo Miron <damian.abalo@cern.ch>
 *      Author: Piotr Nikiel <piotr@nikiel.info>
 *
 *  This file is part of Quasar.
 *
 *  Quasar is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public Licence as published by
 *  the Free Software Foundation, either version 3 of the Licence.
 *
 *  Quasar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public Licence for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Quasar.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <thread>

#include "QuasarServer.h"
#include <LogIt.h>
#include <shutdown.h>
#include <DReliance.h>
#include "GPIO/RelianceBox.h"
#include "GPIO/RelianceFSM.h"
#include <ASReliance.h>

QuasarServer::QuasarServer() : BaseQuasarServer()
{

}

QuasarServer::~QuasarServer()
{
 
}

void QuasarServer::mainLoop()
{
    printServerMsg("Press "+std::string(SHUTDOWN_SEQUENCE)+" to shutdown server");

    RelianceBox * box = RelianceBox::GetInstance();
    box->Startup();

    for(Device::DReliance *rel : Device::DRoot::getInstance()->reliances()){
      RelianceFSM * fsm = new RelianceFSM();
      fsm->SetI2CAddress(rel->I2CAddress());
      fsm->SetAdcChan(rel->Channel());
      fsm->SetThreshold(rel->getAddressSpaceLink()->getThreshold());
      fsm->SetBufferLenC(rel->getAddressSpaceLink()->getLenC());
      fsm->SetBufferLenD(rel->getAddressSpaceLink()->getLenD());
      fsm->SetAdcGain(rel->getAddressSpaceLink()->getGain());
      box->SetFSM(rel->Channel(),fsm);
    }

    // Wait for user command to terminate the server thread.
    while(ShutDownFlag() == 0)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(1000)); //Change 100 ms to 1000 ms

        for(Device::DReliance *rel : Device::DRoot::getInstance()->reliances())
        {
	  RelianceFSM * fsm = box->GetFSM(rel->Channel());
	  rel->getAddressSpaceLink()->setStateMachine(fsm->GetState(), OpcUa_Good);
    rel->getAddressSpaceLink()->setADC(fsm->ReadAdc(), OpcUa_Good);
    rel->getAddressSpaceLink()->setResistance(fsm->ReadResistance(), OpcUa_Good);
    rel->getAddressSpaceLink()->setChasingAverage(fsm->ReadChasingAverage(), OpcUa_Good);
    rel->getAddressSpaceLink()->setAlarm(fsm->GetAlarm(), OpcUa_Good);
    rel->getAddressSpaceLink()->setTimeStamp(fsm->ReadTimeStamp(), OpcUa_Good);
    rel->getAddressSpaceLink()->setLenD(fsm->GetBufferLenD(), OpcUa_Good);
    rel->getAddressSpaceLink()->setLenC(fsm->GetBufferLenC(), OpcUa_Good);
    rel->getAddressSpaceLink()->setGain(fsm->GetAdcGain(), OpcUa_Good);
    rel->getAddressSpaceLink()->setThreshold(fsm->GetThreshold(), OpcUa_Good);
    LOG(Log::INF) << rel->getFullName() << ","
      << " time: " << fsm->ReadTimeStamp() << ","
      << " chan: " << fsm->GetAdcChan() << ","
      << " gain: " << fsm->GetAdcGain() << ","
      << " adc: " << fsm->ReadAdc() << ","
      << " kOhm: " << fsm->ReadResistance() << ","
      << " CAThr: " << fsm->GetThreshold() << ","
      << " CA: " << fsm->ReadChasingAverage()<<","
      << " LenC: " << fsm->GetBufferLenC()<<","
      << " LenD: " << fsm->GetBufferLenD()<<","
      << " State: " << fsm->GetState()<<","
      << " Alarm: " << fsm->GetAlarm()<<", "
      << " Output: " << box->GetOutput(rel->Channel());
        }
        box->CalculateLED();
        box->CalculateOutputs();
        //printServerMsg(" CalculateOutputs");
    }
    printServerMsg(" Shutting down server");
    box->Shutdown();
    delete box;
}

void QuasarServer::initialize()
{
  LOG(Log::INF) << "Initializing Quasar server.";
}

void QuasarServer::shutdown()
{
  LOG(Log::INF) << "Shutting down Quasar server.";
}

void QuasarServer::initializeLogIt()
{
  BaseQuasarServer::initializeLogIt();
  LOG(Log::INF) << "Logging initialized.";
}
